﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace HseSampleProject13.Model
{
    public class Model
    {
        public string Id { get; set; }
        public string Project { get; set; }
        public string Iteration { get; set; }
        public string Created { get; set; }
        public List<Prediction> Predictions { get; set; }
    }

    public class Prediction
    {
        public string TagId { get; set; }
        public string Tag { get; set; }
        public double Probability { get; set; }
    }

    public class AccountInfo
    {
        public AccountInfo()
        {
            this.Eth = new EthAccountToken();
            this.AccountTokens = new List<AccountToken>();
        }

        public string Address { get; set; }
        public AccountToken Eth { get; set; }
        public List<AccountToken> AccountTokens { get; set; }
    }

    public class AccountToken
    {
        public string Symbol { get; set; }
        public decimal Balance { get; set; }
    }

    public class EthAccountToken : AccountToken
    {
        public EthAccountToken()
        {
            this.Symbol = "ETH";
        }
    }
    public class WalletSummary
    {
        public List<AccountInfo> AccountsInfo { get; private set; }
        public List<AccountToken> TokenBalanceSummary { get; private set; }

        public EthAccountToken EthBalanceSummary { get; private set; }

        public WalletSummary(List<AccountInfo> accountsInfo)
        {
            this.AccountsInfo = accountsInfo;
            InitialiseEthBalanceSummary();
            InitialiseTokenBalanceSummary();
        }

        private void InitialiseEthBalanceSummary()
        {
            EthBalanceSummary = new EthAccountToken { Balance = AccountsInfo.Sum(x => x.Eth.Balance) };
        }

        private void InitialiseTokenBalanceSummary()
        {
            TokenBalanceSummary = new List<AccountToken>();
            var tokens = AccountsInfo.SelectMany(x => x.AccountTokens);
            var symbols = tokens.Select(x => x.Symbol).Distinct();

            foreach (var symbol in symbols)
            {
                var tokenSummary = new AccountToken() { Symbol = symbol };
                tokenSummary.Balance = tokens.Where(x => x.Symbol == symbol).Sum(x => x.Balance);
                TokenBalanceSummary.Add(tokenSummary);
            }
        }
    }

    public class MasterPageItem
    {
        public string Text { get; set; }
        public Type Type { get; set; }
    }

    public class WalletTransaction
    {
        public string From { get; set; }
        public string To { get; set; }
        public string Token { get; set; }
        public decimal Amount { get; set; }
    }

    public interface IAccountTokenViewModelMapperService
    {
        List<AccountTokenViewModel> Map(List<AccountToken> accountTokens);
        List<AccountTokenViewModel> Map(EthAccountToken ethAccountToken, List<AccountToken> accountTokens);
    }
}