﻿using HseSampleProject13.Pages;
using Xamarin.Forms;
namespace HseSampleProject13
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();
            //MainPage = new NavigationPage(new MainPage());
            //MainPage = new NavigationPage(new BlockChain());
            //MainPage = new BlockChain();
            MainPage = new MasterDetailPageNavigation();
        }

        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
