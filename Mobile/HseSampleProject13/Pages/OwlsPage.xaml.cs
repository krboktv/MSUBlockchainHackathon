﻿using System;
using System.Collections.Generic;
using HseSampleProject13.ViewModel;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
namespace HseSampleProject13.Pages
{
    public partial class OwlsPage : ContentPage
    {
        private static OwlsViewModel VM = new OwlsViewModel();
        public OwlsPage()
        {
            InitializeComponent();
            BindingContext = VM;
        }
    }
}
