﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using HseSampleProject13.Model;
using PropertyChanged;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace HseSampleProject13.Pages
{
    public partial class MasterPage : ContentPage
    {
        private static MasterViewModel VM = new MasterViewModel();
       
        public ListView ListView { get { return listView; } }

        public MasterPage()
        {
            InitializeComponent();
            BindingContext = VM;
            this.Title = "Master";
        }

    }
    [AddINotifyPropertyChangedInterface]
    public class MasterViewModel
    {
        public MasterViewModel()
        {
            ListViewSource = new ObservableCollection<MasterPageItem>()
            {
                new MasterPageItem(){Text="Live Chat", Type=typeof(BotView)},
                new MasterPageItem(){Text="Account", Type=typeof(BlockChain)}
            };
        }
        public ObservableCollection<MasterPageItem> ListViewSource { get; set; }
    }
}
