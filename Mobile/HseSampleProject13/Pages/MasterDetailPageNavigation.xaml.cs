﻿using System;
using System.Collections.Generic;
using HseSampleProject13.Model;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace HseSampleProject13.Pages
{
    public partial class MasterDetailPageNavigation : MasterDetailPage
    {
        private static MasterPage mp = new MasterPage();
        public MasterDetailPageNavigation()
        {
            InitializeComponent();
            this.Master = mp;
            Detail = new NavigationPage((Page) Activator.CreateInstance(typeof(MainPage)));
           
            mp.ListView.ItemSelected += Handle_ItemSelected;
        }

        void Handle_ItemSelected(object sender, Xamarin.Forms.SelectedItemChangedEventArgs e)
        {
            ListView tmp = sender as ListView;
            if (tmp != null)
            {
                var item = tmp.SelectedItem as MasterPageItem;
                tmp.SelectedItem = null;
                if (item != null)
                {
                    Detail = new NavigationPage((Page) Activator.CreateInstance(item.Type));
                    mp.ListView.SelectedItem = null;
                    IsPresented = false;
                }
            }
        }
    }
}
