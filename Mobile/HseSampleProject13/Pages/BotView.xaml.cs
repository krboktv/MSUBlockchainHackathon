﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Xamarin.Forms;

namespace HseSampleProject13.Pages
{
    public partial class BotView : ContentPage
    {
        public BotView()
        {
            InitializeComponent();
        }
        private string conversationId;
        private string token;
        private HttpClient _httpClient;

        public async Task<bool> Setup()
        {
            _httpClient = new HttpClient();
            _httpClient.BaseAddress = new Uri("https://directline.botframework.com/");
            _httpClient.DefaultRequestHeaders.Accept.Clear();
            _httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            _httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", "<bot secret key>");
            var response = await _httpClient.PostAsync("/api/tokens/conversation", null);

            if (response.IsSuccessStatusCode)
            {
                var result = response.Content.ReadAsStringAsync();
                token = JsonConvert.DeserializeObject<string>(result.Result);
                return true;
            }
            return false;
        }


        public class BotMessage
        {
            public string Id { get; set; }
            public string ConversationId { get; set; }
            public DateTime Created { get; set; }
            public string From { get; set; }
            public string Text { get; set; }
            public string ChannelData { get; set; }
            public string[] Images { get; set; }
            public Attachment[] Attachments { get; set; }
            public string ETag { get; set; }
        }

        public class Attachment
        {
            public string Url { get; set; }
            public string ContentType { get; set; }
        }

        public class BotMessageRoot
        {
            public List<BotMessage> Messages { get; set; }
        }
    }
}
