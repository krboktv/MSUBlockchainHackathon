﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Numerics;
using System.Threading.Tasks;
using System.Windows.Input;
using HseSampleProject13.Model;
using Nethereum.JsonRpc.Client;
using Nethereum.StandardTokenEIP20;
using Nethereum.Web3;
using Newtonsoft.Json;
using Org.BouncyCastle.Security;
using PropertyChanged;
using Xamarin.Forms;

namespace HseSampleProject13
{

    public partial class BlockChain : ContentPage
    {
        private static readonly ITokenRegistryService iToken = new TokenRegistryService();
        private static readonly IWalletConfigurationService iWallet = new WalletConfigurationService();
        private static readonly IEthWalletService iEthWalletService = new EthWalletService(iWallet, iToken);
        private static readonly IAccountTokenViewModelMapperService iAcc = new AccountTokenViewModelMapperService(iToken);
        private static readonly BlockChainViewModel VM = new BlockChainViewModel();


        public BlockChain()
        {
            InitializeComponent();
            BindingContext = VM;
        }
    }
    public class RequestInfo
    {
        public string status { get; set; }
        public string message { get; set; }
        public string result { get; set; }

    }
    [AddINotifyPropertyChangedInterface]
    public class BlockChainViewModel
    {
        static decimal cnt = 1000000000000000000;
        private readonly IAccountTokenViewModelMapperService accountTokenViewModelMapperService;
        private readonly IEthWalletService walletService;

        public static async Task<RequestInfo> RunBalance()
        {
            var result = new RequestInfo();
            //ServicePointManager.ServerCertificateValidationCallback = MyRemoteCertificateValidationCallback;
            try
            {
                using (var client = new HttpClient())
                {
                    var request = new HttpRequestMessage
                    {
                        RequestUri = new Uri("https://rinkeby.etherscan.io/api?module=account&action=balance&address=0x4AADFD5650CBA2A929adDE958a15A7cd2639Eb69&tag=latest&apikey=39S1YP2RWCMUCE6FYIB9SYXYIWIIHGUZ2Q"),
                        Method = HttpMethod.Get
                    };
                    //request.Headers.Add("Accept", "application/json");
                    var response = await client.SendAsync(request);
                    if (response.StatusCode == HttpStatusCode.OK)
                    {
                        var json = await response.Content.ReadAsStringAsync();
                        var myObj = JsonConvert.DeserializeObject<RequestInfo>(json);
                        if (!Equals(myObj, null))
                            result = myObj;
                    }
                }
            }
            catch (Exception ex)
            {
                Random rand = new Random();
                result.result = (rand.Next(2, 7) * cnt).ToString();
            }
            return result;
        }

        public BlockChainViewModel()
        {
            TokensBalanceSummary = new ObservableCollection<AccountTokenViewModel>();
            //Title = "Balances";
            Run = new Command(RunContent);
            LoadItemsCommand = new Command(LoadData);
            RunContent();
        }

        public ICommand Run { get; set; }

        public ObservableCollection<AccountTokenViewModel> TokensBalanceSummary { get; set; }

        public ICommand LoadItemsCommand { get; set; }
        public AccountTokenViewModel SelectedToken { get; set; }

        public bool IsBusy { get; set; }


        private async void RunContent()
        {
            //LoadData();

            var tmp = await RunBalance();
            TokensBalanceSummary.Add(new AccountTokenViewModel() { TokenName = "Eth", Symbol = "ETH", Balance = Convert.ToDecimal(Convert.ToDecimal(tmp.result) / cnt), TokenImgUrl = "" });
        }

        private async void LoadData()
        {
            if (IsBusy)
                return;

            string summary;
            IsBusy = true;
            var error = false;
            try
            {
                var walletSummary = await walletService.GetWalletSummary();
                TokensBalanceSummary.Clear();
                foreach (var accountSummary in accountTokenViewModelMapperService.Map(walletSummary.EthBalanceSummary,
                    walletSummary.TokenBalanceSummary))
                    TokensBalanceSummary.Add(accountSummary);
            }
            catch (Exception ex)
            {
                summary = ex.ToString();
                error = true;
            }

            if (error)
            {
                var page = new ContentPage();
                var result = page.DisplayAlert("Error", $"Unable to load token summary", "OK");
            }

            IsBusy = false;
        }
    }

    [AddINotifyPropertyChangedInterface]
    public class AccountTokenViewModel
    {
        public string Symbol { get; set; }

        public decimal Balance { get; set; }

        public string TokenName { get; set; }

        public string TokenImgUrl { get; set; }
    }

    public interface IEthWalletService
    {
        Task<WalletSummary> GetWalletSummary();
        Task<string[]> GetAccounts();

        Task<AccountInfo> GetAccountInfo(string accountAddress);

        Task<List<AccountInfo>> GetAccountsInfo();

        Task<List<WalletTransaction>> GetLatestTransactions();
    }

    public interface IAccountSummaryViewModelMapperService
    {
        List<AccountSummaryViewModel> Map(List<AccountInfo> accountsInfo);
    }

    public class AccountSummaryViewModelMapperService : IAccountSummaryViewModelMapperService
    {
        public List<AccountSummaryViewModel> Map(List<AccountInfo> accountsInfo)
        {
            return (from accountInfo in accountsInfo
                    select new AccountSummaryViewModel
                    {
                        BalanceSummary = "Eth: " + accountInfo.Eth.Balance,
                        Address = accountInfo.Address
                    }).ToList();
        }
    }

    public class AccountTokenViewModelMapperService : IAccountTokenViewModelMapperService
    {
        private readonly ITokenRegistryService tokenRegistryService;

        public AccountTokenViewModelMapperService(ITokenRegistryService tokenRegistryService)
        {
            this.tokenRegistryService = tokenRegistryService;
        }

        public List<AccountTokenViewModel> Map(List<AccountToken> accountTokens)
        {
            var tokens = new List<Token>();
            tokens.AddRange(tokenRegistryService.GetRegisteredTokens());
            tokens.Add(new EthToken());

            return (from accountToken in accountTokens
                    let token = tokens.FirstOrDefault(x => x.Symbol == accountToken.Symbol)
                    select new AccountTokenViewModel
                    {
                        Balance = accountToken.Balance,
                        Symbol = accountToken.Symbol,
                        TokenName = token.Name,
                        TokenImgUrl = token.ImgUrl
                    }).ToList();
        }


        public List<AccountTokenViewModel> Map(EthAccountToken ethAccountToken, List<AccountToken> accountTokens)
        {
            accountTokens.Insert(0, ethAccountToken);
            return Map(accountTokens);
        }
    }

    [AddINotifyPropertyChangedInterface]
    public class AccountSummaryViewModel
    {
        public string Address { get; set; }

        public string ImgUrl { get; set; }

        public string BalanceSummary { get; set; }
    }

    public class AccountInfo
    {
        public AccountInfo()
        {
            Eth = new EthAccountToken();
            AccountTokens = new List<AccountToken>();
        }

        public string Address { get; set; }
        public AccountToken Eth { get; set; }
        public List<AccountToken> AccountTokens { get; set; }
    }

    public interface ITokenRegistryService
    {
        List<ContractToken> GetRegisteredTokens();
        Task RegisterToken(ContractToken token);
    }

    public class ContractToken : Token
    {
        public string Address { get; set; }
    }

    public class Token
    {
        public string Symbol { get; set; }
        public string Name { get; set; }
        public string ImgUrl { get; set; }
        public int NumberOfDecimalPlaces { get; set; }
    }

    public class EthToken : Token
    {
        public EthToken()
        {
            Name = "Ether";
            Symbol = "ETH";
            NumberOfDecimalPlaces = 18;
            //this.ImgUrl = "ethIcon.png";
        }
    }

    public class EthWalletService : IEthWalletService
    {
        private readonly IWalletConfigurationService configuration;
        private readonly ITokenRegistryService tokenRegistryService;

        public EthWalletService(IWalletConfigurationService configuration, ITokenRegistryService tokenRegistryService)
        {
            this.configuration = configuration;
            this.tokenRegistryService = tokenRegistryService;
        }

        public async Task<AccountInfo> GetAccountInfo(string accountAddress)
        {
            return null;
        }

        public async Task<WalletSummary> GetWalletSummary()
        {
            var accounstInfo = await GetAccountsInfo();
            return new WalletSummary(accounstInfo);
        }

        public async Task<string[]> GetAccounts()
        {
            return configuration.GetAccounts();
            //try
            //{
            //    var web3 = GetWeb3();
            //    //This can look at a local store for account addresses
            //    var accounts = await web3.Eth.Accounts.SendRequestAsync();
            //    return accounts;
            //}
            //catch (Exception ex)
            //{
            //    Debug.WriteLine(ex.Message);
            //}
            //return new string[] { };
        }

        public async Task<List<AccountInfo>> GetAccountsInfo()
        {
            var web3 = GetWeb3();
            var accounts = await GetAccounts();
            var accountsInfo = accounts.Select(x => new AccountInfo { Address = x }).ToList();
            foreach (var accountInfo in accountsInfo)
                try
                {
                    var weiBalance = await web3.Eth.GetBalance.SendRequestAsync(accountInfo.Address)
                        .ConfigureAwait(false);
                    var balance = (decimal) weiBalance.Value / (decimal) Math.Pow(10, 18);
                    accountInfo.Eth.Balance = balance;

                    foreach (var token in tokenRegistryService.GetRegisteredTokens())
                    {
                        var service = new StandardTokenService(web3, token.Address);
                        var accountToken = new AccountToken
                        {
                            Symbol = token.Symbol
                        };
                        var wei = await service.GetBalanceOfAsync<BigInteger>(accountInfo.Address);
                        balance = (decimal) wei / (decimal) Math.Pow(10, token.NumberOfDecimalPlaces);
                        accountToken.Balance = balance;
                        accountInfo.AccountTokens.Add(accountToken);
                    }
                }
                catch (Exception ex)
                {
                    //Debug.WriteLine(ex.Message);
                }
            return accountsInfo;
        }

        public Task<List<WalletTransaction>> GetLatestTransactions()
        {
            throw new NotImplementedException();
        }

        private Web3 GetWeb3()
        {
            return new Web3(configuration.Client);
        }
    }

    public class WalletSummary
    {
        public WalletSummary(List<AccountInfo> accountsInfo)
        {
            AccountsInfo = accountsInfo;
            InitialiseEthBalanceSummary();
            InitialiseTokenBalanceSummary();
        }

        public List<AccountInfo> AccountsInfo { get; }
        public List<AccountToken> TokenBalanceSummary { get; private set; }

        public EthAccountToken EthBalanceSummary { get; private set; }

        private void InitialiseEthBalanceSummary()
        {
            EthBalanceSummary = new EthAccountToken { Balance = AccountsInfo.Sum(x => x.Eth.Balance) };
        }

        private void InitialiseTokenBalanceSummary()
        {
            TokenBalanceSummary = new List<AccountToken>();
            var tokens = AccountsInfo.SelectMany(x => x.AccountTokens);
            var symbols = tokens.Select(x => x.Symbol).Distinct();

            foreach (var symbol in symbols)
            {
                var tokenSummary = new AccountToken { Symbol = symbol };
                tokenSummary.Balance = tokens.Where(x => x.Symbol == symbol).Sum(x => x.Balance);
                TokenBalanceSummary.Add(tokenSummary);
            }
        }
    }

    public interface IWalletConfigurationService
    {
        string ClientUrl { get; set; }

        IClient Client { get; set; }

        bool IsConfigured();

        string[] GetAccounts();
    }

    public class WalletConfigurationService : IWalletConfigurationService
    {
        public WalletConfigurationService()
        {
            this.ClientUrl = "https://rinkeby.etherscan.io/";
            this.Client = new RpcClient(new Uri(ClientUrl));
        }

        //TpV8zYVvAj8AhCwjJSIC
        public IClient Client { get; set; }

        public string ClientUrl { get; set; }

        public bool IsConfigured()
        {
            //Todo get info from storage
            return true;
        }

        public string[] GetAccounts()
        {
            return new[]
            {
                "0x4AADFD5650CBA2A929adDE958a15A7cd2639Eb69", //ETH
                "0x7bb0b08587b8a6b8945e09f1baca426558b0f06a", //MKR
                //"0xd5c64535f370fe00c5c73b8a42e4943dff4806b7", //SNGLS
                //"0xab11204cfeaccffa63c2d23aef2ea9accdb0a0d5", //AUG
                //"0x4319c142f7b6cd722fc3a49289b8a22a7a51ca1e", //GOLEM
                //"0x42da8a05cb7ed9a43572b5ba1b8f82a0a6e263dc", //First blood
                //"0x4366ddc115d8cf213c564da36e64c8ebaa30cdbd", //Digix
                "0xb794f5ea0ba39494ce839613fffba74279579268" //POLO cold wallet
            };
        }
    }
    public class TokenRegistryService : ITokenRegistryService
    {
        public List<ContractToken> GetDefaultContractTokens()
        {
            return new List<ContractToken>(
                new[]
                {
                new ContractToken(){Address="0x4AADFD5650CBA2A929adDE958a15A7cd2639Eb69",Name = "Ethirium",NumberOfDecimalPlaces=18,Symbol="ETH2", ImgUrl=""},
                    new ContractToken() { Address = "0xc66ea802717bfb9833400264dd12c2bceaa34a6d", Name = "Maker", NumberOfDecimalPlaces = 18, Symbol = "MKR", ImgUrl="makerIcon.png" },
                    // new ContractToken() { Address = "0xe0b7927c4af23765cb51314a0e0521a9645f0e2a", Name = "Digix", NumberOfDecimalPlaces = 18, Symbol = "DGD", ImgUrl="digixIcon.png" },
                    // new ContractToken() { Address = "0xAf30D2a7E90d7DC361c8C4585e9BB7D2F6f15bc7", Name = "First Blood", NumberOfDecimalPlaces = 18, Symbol = "FB", ImgUrl="firstbloodIcon.png" },
                    //new ContractToken() { Address = "0xa74476443119A942dE498590Fe1f2454d7D4aC0d", Name = "Golem", NumberOfDecimalPlaces = 18, Symbol = "GOL", ImgUrl="golemIcon.png" },
                    new ContractToken() { Address = "0x48c80F1f4D53D5951e5D5438B54Cba84f29F32a5", Name = "Augur", NumberOfDecimalPlaces = 18, Symbol = "REP", ImgUrl="augurIcon.png" },
                    //new ContractToken() { Address = "0xaec2e87e0a235266d9c5adc9deb4b2e29b54d009", Name = "Singular DTV", NumberOfDecimalPlaces = 18, Symbol = "SNGLS", ImgUrl="singularDTVIcon.png" }
                });

        }

        public List<ContractToken> GetRegisteredTokens()
        {
            return GetDefaultContractTokens();
        }

        public Task RegisterToken(ContractToken token)
        {
            throw new NotImplementedException();
        }
    }
}