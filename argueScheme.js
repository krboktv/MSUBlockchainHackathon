var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var Argue = new Schema(
    {   
        address1: {
            type: String,
            default: ""
        },
        address2: {
            type: String,
            default: ""
        },
        sum: {
            type: String,
            default: ""
        },
        name: {
            type: String,
            default: "Пустой спор"
        },
        active: {
            type: Boolean,
            default: true
        },
        val1: {
            type: Number,
            default: 0
        },
        val2: {
            type: Number,
            default: 0
        },
        setDate1: {
            type: String
        },
        actualTime: {
            type: String,
        },
        winner: {
            type: String,
            default: ""
        }
    },
    {versionKey: false}
);

module.exports = mongoose.model('Argue', Argue);  
