pragma solidity ^0.4.4;

contract ERC20 {
    event Transfer(address indexed _from, address indexed _to, uint256 _value);
    event Approval(address indexed _owner, address indexed _spender, uint256 _value);

    string public constant name      = "ERC20 Token"; 
    string public constant symbol    = "ERC20";               
    uint256 public constant decimals = 18;                  
    uint256 public totalSupply;
    
    mapping (address => uint256) balances;
    mapping (address => mapping (address => uint256)) allowed;

    function transfer(address _to, uint256 _value) returns (bool success) {
        require(balances[msg.sender] >= _value);
        balances[msg.sender] -= _value;
        balances[_to] += _value;
        Transfer(msg.sender, _to, _value);
        return true;
    }

    function transferFrom(address _from, address _to, uint256 _value) returns (bool success) {
        require(balances[_from] >= _value && allowed[_from][msg.sender] >= _value);
        balances[_to] += _value;
        balances[_from] -= _value;
        allowed[_from][msg.sender] -= _value;
        Transfer(_from, _to, _value);
        return true;
    }

    function balanceOf(address _owner) constant returns (uint256 balance) {
        return balances[_owner];
    }

    function approve(address _spender, uint256 _value) returns (bool success) {
        allowed[msg.sender][_spender] = _value;
        Approval(msg.sender, _spender, _value);
        return true;
    }

    function allowance(address _owner, address _spender) constant returns (uint256 remaining) {
      return allowed[_owner][_spender];
    }

    function mint(address _to, uint256 _value) public {
        assert(totalSupply + _value >= totalSupply && balances[_to] + _value >= balances[_to]); 
        balances[_to] += _value;                                                                
        totalSupply += _value;                                                                  
    }

        
    
    mapping (uint256 => uint256) private game_unixtime;
    mapping (uint256 => address[]) private game_player;
    mapping (uint256 => uint256[]) private game_bet;
    mapping (uint256 => uint256[]) private game_prediction;
    mapping (uint256 => uint256) private game_fact;

    
    function betPredictionTime(uint256 bet, uint256 prediction, uint256 unixtime) public {
        if (unixtime <= now + 216000) return;   // 1 hour = 2160000
        uint256 i;
        while (game_unixtime[i] != 0) {
            if (game_unixtime[i] == unixtime) break;
            ++i;
        }
        game_player[i].push(msg.sender);
        game_bet[i].push(bet);
        game_prediction[i].push(prediction);
    }
    
    
    function factTime(uint256 fact, uint256 unixtime) public {
        if (unixtime > now) return;   // 1 hour = 2160000
        uint256 i;
        while (game_unixtime[i] != 0) {
            if (game_unixtime[i] == unixtime) break;
            ++i;
        }
        game_fact[i] = fact;
    }
    
 
}