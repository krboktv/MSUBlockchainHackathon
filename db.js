var mongoose = require("mongoose");
var ObjectID = require("mongodb").ObjectID;
var Argue = require('./argueScheme.js');
var request = require('request');


mongoose.Promise = global.Promise;

var uri = 'mongodb://52.224.235.2:27017/db';

var options = {
    useMongoClient: true,
    autoIndex: true, 
    reconnectTries: Number.MAX_VALUE, // Never stop trying to reconnect
    reconnectInterval: 500, // Reconnect every 500ms
    poolSize: 10, // Maintain up to 10 socket connections
    // If not connected, return errors immediately rather than waiting for reconnect
    bufferMaxEntries: 0
  };
var db = mongoose.connect(uri, options).then(console.log('ZBS PODRUBIL!!1'));

function addValuesToArgue(nameArgue,sum,address1,val1,setDate1) {
    var time1 = new Date();
    var time = Date.parse(time1).toString();
    var actualDate = time.substring(0,time.length-3).toString();
    
    Argue.create({address1: address1, sum: sum, name: nameArgue, val1: val1, setDate1: setDate1},function(err,doc){
        if(err) return console.log(err);
        console.log("Добавлен Спор:", doc);
    })
        .then(function(res) {
                setTimeout(function() {

                        request('https://api.cryptonator.com/api/ticker/waves-usd', function (error, response, body) {
                            var price = JSON.parse(body).ticker.price;
                            console.log("Цена вейвса"+price);
                            Argue.find({_id: res._id}, function(err,res) {
                                if(err) return console.log(err);
                                console.log("Найденные споры:", res);
                            })
                                .then(function(res1) {
                                    var winner;
                                    if(Math.abs(Number(price) - res1[0].val1)<Math.abs(Number(price) - res1[0].val2)) {
                                        console.log('The first is win');
                                        winner = res1[0].address1;
                                        
                                    }
                                    else if (Math.abs(Number(price) - res1[0].val1)>Math.abs(Number(price) - res1[0].val2)) {
                                        console.log('The second is win');
                                        winner = res1[0].address2;
                                    }
                                    else {
                                        console.log('Ничья');
                                        winner = 'Ничья';
                                    }
                                    Argue.update({_id: ObjectID(res1[0]._id)},{winner: winner, active: false}, function(err,res) {
                                        if(err) return console.log(err);
                                        console.log("Спор обновлён:", res);
                                    });
                                });
                        });
    
                        var time12 = new Date();
                        console.log('Спор выигран или проигран в: '+time12);
                }, (Number(res.setDate1)-Number(actualDate))*1000);
        });
}

function findArgues() {
    return new Promise(function(resolve, reject) {
        Argue.find({}, function(err,res) {
            if(err) return console.log(err);
            console.log("Найденные споры:", res);
        })
            .then(function(res) {
                resolve(res);
            });
    });
}

function updateArgue(address2, val2, id) {
    Argue.update({_id: ObjectID(id)},{address2: address2, val2: val2}, function(err,res) {
        if(err) return console.log(err);
        console.log("Спор обновлён:", res);
    });
}

module.exports.updateArgue = updateArgue;
module.exports.addValuesToArgue = addValuesToArgue;
module.exports.findArgues = findArgues;

