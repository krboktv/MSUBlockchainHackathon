var restify = require('restify');
var builder = require('botbuilder');
var justDB = require("./db.js");
var mongoose = require("mongoose");
var ObjectID = require("mongodb").ObjectID;
var Argue = require('./argueScheme.js');

// Setup Restify Server
var server = restify.createServer();
server.listen(process.env.port || process.env.PORT || 3978, function () {
   console.log('%s listening to %s', server.name, server.url); 
});



var inMemoryStorage = new builder.MemoryBotStorage();

// Create chat connector for communicating with the Bot Framework Service
var connector = new builder.ChatConnector({
    appId: "3eb8f28c-51ca-45fb-a328-71abb8f8c201",
    appPassword: "orcTX084;*_ykgnSJENA86$"
});

// Listen for messages from users 
server.post('/api/messages', connector.listen());

var bot = new builder.UniversalBot(connector, [
    function (session) {
        session.send("Привет! Я помогу вам сделать честный спор:)");
        session.beginDialog('rootMenu');
    },
    function (session, results) {
        session.endConversation("Будет спор - приходи:)");
    }
]).set('storage', inMemoryStorage); // Register in memory storage

// Add root menu dialog
bot.dialog('rootMenu', [
    function (session) {
        builder.Prompts.choice(session, "Выберете пункт:", 'Создать спор|Принять участие в споре|Просмотреть активные споры|Завершённые споры|Выход');
    },
    function (session, results) {
        switch (results.response.index) {
            case 0:
                session.beginDialog('CreateArgue');
                break;
            case 1:
                session.beginDialog('takePlaceInArgue');
                break;
            case 2:
                session.beginDialog('findArgues');
                break;
            case 3:
                session.beginDialog('findCompletedArgues');
                break;
            default:
                session.endDialog();
                break;
        }
    },
    function (session) {
        // Reload menu
        session.replaceDialog('rootMenu');
    }
]).reloadAction('showMenu', null, { matches: /^(menu|back)/i });

// Flip a coin
bot.dialog('CreateArgue', [
    function (session, args) {
        builder.Prompts.text(session, "Введите название спора");
    },
    function(session, results) {
        if (results.response != "") {
            session.userData.name = results.response;
            builder.Prompts.number(session, "Введите сумму в MSUToken, на которую будете спорить");
        } else {
            session.endDialog("Ошибочка:( Вы не ввели имя.");
        }
    },
    function (session, results) {
        session.userData.count = results.response;
        builder.Prompts.text(session, "Вы будете спорить на: " + results.response + " MSU. Теперь введить Ethereum адрес."); 
    },
    function (session, results) {
        session.userData.address = results.response;
        session.send("Перейдём к спору. Вам нужно сделать прогноз цены 1 Waves (в USD) на любое число."); 
        builder.Prompts.text(session, "Введите предположительную стоимость 1 Waves"); 
    },
    function (session, results) {
        session.userData.val1 = results.response;
        session.send("Вы считаете, что 1 Waves будет стоить " + results.response + " USD"); 
        builder.Prompts.time(session, "Какого числа это будет? (в формате ММ/ДД/ГГ ЧЧ/ММ/СС)"); 
    },
    function (session, results) {
        var time = builder.EntityRecognizer.resolveTime([results.response]).getTime().toString();
        session.dialogData.time = time.substring(0,time.length-3);
        justDB.addValuesToArgue(session.userData.name,session.userData.count,session.userData.address,session.userData.val1, session.dialogData.time);
        session.endDialog("Ваш спор создан");
    }
]);

var argues = {};
var sum;

bot.dialog('takePlaceInArgue', [
    function (session) {
        var t = justDB.findArgues()
        .then(function(res) {
            for(var key in res)  {
                if((res[key].active == true) && (res[key].address2 == '')) {
                    if(argues[res[key].name+', '+res[key].sum+'MSU']) {
                        argues[res[key].name+', '+res[key].sum+'MSU'+"("+key+")"] = res[key];
                    }
                    else {
                        argues[res[key].name+', '+res[key].sum+'MSU'] = res[key];
                    }
                }
            }
            if(Object.keys(argues).length>0) {
                builder.Prompts.choice(session, "Вы можете принять участие в споре:", argues);
            }
            else {
                session.endDialog('Нет активных споров');
            }
        });
    },
    function(session, results) {
        if((Number.isInteger(results.response.index)) && (Object.keys(argues).length >= (results.response.index+1))) {
            session.userData.nameArgue = results.response.entity;
            var myDate = new Date(Number(argues[session.userData.nameArgue].setDate1+'000'));

            session.send(
                "Ваш оппонент считает, что "+
                (myDate.getMonth()+1)+'/'+
                myDate.getDate()+'/'+
                myDate.getFullYear()+' в '+
                myDate.getHours()+':'+
                myDate.getMinutes()+':'+
                myDate.getSeconds()+
                " 1 Waves будет стоить "+argues[session.userData.nameArgue].val1+
                " USD. Сумма спора: "+argues[session.userData.nameArgue].sum+ ' MSU'
                ); 
            builder.Prompts.choice(session, "Вы гототовы поспорить с ним, что 1 WAVES будет иметь другую стоимость?", "Да|Нет"); 
        }
        else {
            session.endDialog('Такого спора не существует!');
            argues = {};
        }
    },
    function (session, results) {
        if(results.response.index == 0) {
            builder.Prompts.text(session, "Введите ваш адрес Ethereum"); 
        }
        else if(results.response.index == 1) {
            session.endDialog('Вы отказались спорить');
        }
        else 
            session.endDialog('Вы ввели некорректное значение');

    },
    function (session, results) {
        session.userData.address2 = results.response;
        builder.Prompts.text(session, "Введите предположительную стоимость 1 Waves"); 
    },
    function(session, results) {
        session.userData.val2 = results.response;
        justDB.updateArgue(session.userData.address2,session.userData.val2, argues[session.userData.nameArgue]._id);
        session.endDialog('Вы участвуете в споре!');
        argues = {};
    }
]);

bot.dialog('findArgues', [
    function (session) {
        var t = justDB.findArgues()
        .then(function(res) {
            for(var key in res)  {
                if((res[key].active == true) && (res[key].address2 != '')) {
                    if(argues[res[key].name+', '+res[key].sum+'MSU']) {
                        argues[res[key].name+', '+res[key].sum+'MSU'+"("+key+")"] = res[key];
                    }
                    else {
                        argues[res[key].name+', '+res[key].sum+'MSU'] = res[key];
                    }
                }
            }
            if(Object.keys(argues).length>0) {
                builder.Prompts.choice(session, "Вы можете посмотреть активные споры:", argues);
            }
            else {
                session.endDialog('Нет активных споров');
            }
        });
    },
    function(session, results) {
        var myDate = new Date(Number(argues[results.response.entity].setDate1)+'000');
        session.endDialog( 
            'Название спора: '+
            argues[results.response.entity].name+'. Сумма спора: '+
            argues[results.response.entity].sum+'. Адрес первого участника: '+
            argues[results.response.entity].address1+'. Стоимость 1 WAVES первого участника: '+
            argues[results.response.entity].val1+'. Адрес второго участника: '+
            argues[results.response.entity].address2+'. Стоимость 1 WAVES участника: '+
            argues[results.response.entity].val2+'. Дата окончания спора: '+
            (myDate.getMonth()+1)+'/'+
            myDate.getDate()+'/'+
            myDate.getFullYear()+' в '+
            myDate.getHours()+':'+
            myDate.getMinutes()+':'+
            myDate.getSeconds()
        ); 
        argues = {};
    }
]);

bot.dialog('findCompletedArgues', [
    function (session) {
        var t = justDB.findArgues()
        .then(function(res) {
            for(var key in res)  {
                if((res[key].active == false) && (res[key].address2 != '')) {
                    if(argues[res[key].name+', '+res[key].sum+'MSU']) {
                        argues[res[key].name+', '+res[key].sum+'MSU'+"("+key+")"] = res[key];
                    }
                    else {
                        argues[res[key].name+', '+res[key].sum+'MSU'] = res[key];
                    }
                }
            }
            if(Object.keys(argues).length>0) {
                builder.Prompts.choice(session, "Вы можете посмотреть активные споры:", argues);
            }
            else {
                session.endDialog('Нет завершённых споров споров');
            }
        });
    },
    function(session, results) {
        var myDate = new Date(Number(argues[results.response.entity].setDate1+'000'));
        session.endDialog( 
            'Название спора: '+
            argues[results.response.entity].name+'. Сумма спора: '+
            argues[results.response.entity].sum+'. Адрес победителя: '+
            argues[results.response.entity].winner+'. Дата окончания спора: '+
            (myDate.getMonth()+1)+'/'+
            myDate.getDate()+'/'+
            myDate.getFullYear()+' в '+
            myDate.getHours()+':'+
            myDate.getMinutes()+':'+
            myDate.getSeconds()
        ); 
        argues = {};
    }
]);